# Frontend, the external part of HAIDI's software.

This part of code is Django-based and handles the static files and html pages
needed to show the debug panel and the robot's screen. It even contains the speech
recognition database.
