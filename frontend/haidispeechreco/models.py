# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class Language(models.Model):
    name = models.CharField(max_length=10, verbose_name=_("Name"))

    api_code = models.CharField(max_length=20, verbose_name=_("API Language code"), unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", )
        verbose_name = _("language")
        verbose_name_plural = _("languages")


class Action(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("Name"), unique=True)

    code = models.TextField(
        verbose_name=_("Python code"),
        help_text=_("Override the \"response\" variable to change the default response"),
        null=True,
        blank=True
    )

    emotions =  [
        ["1", _("All leds ON")],
        ["2", _("All leds OFF")],
        ["3", _("Happy")],
        ["4", _("Sad")],
        ["5", _("Angry")]
    ]

    emotion = models.CharField(max_length=1, choices=emotions)

    @property
    def emotion_name(self):
        for i in self.emotions:
            if i[0] == self.emotion:
                return i[1]

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", )
        verbose_name = _("action")
        verbose_name_plural = _("actions")


class Keyphrase(models.Model):
    action = models.ForeignKey(Action, verbose_name=_("Action"), on_delete=models.CASCADE)

    language = models.ForeignKey(Language, verbose_name=_("Language"), on_delete=models.CASCADE)

    phrase = models.TextField(verbose_name=_("Phrase"))

    def __str__(self):
        return "%s (%s)" % (self.phrase, self.action.name)

    def clean(self):
        if Keyphrase.objects.filter(language=self.language, phrase=self.phrase).exists():
            raise ValidationError("Another keyphrase with the same language and phrase already exists.")

    class Meta:
        ordering = ("action", "language", "phrase")
        verbose_name = _("keyphrase")
        verbose_name_plural = _("keyphrases")


class Answer(models.Model):
    action = models.ForeignKey(Action, verbose_name=_("Action"), on_delete=models.CASCADE)

    language = models.ForeignKey(Language, verbose_name=_("Language"), on_delete=models.CASCADE)

    answer = models.TextField(verbose_name=_("Answer"))

    custom_answer = models.TextField(verbose_name=_("Custom answer"), null=True, blank=True)

    def __str__(self):
        return "%s (%s)" % (self.action.name, self.language.name)

    def clean(self):
        if Answer.objects.filter(language=self.language, answer=self.answer).exists():
            raise ValidationError("Another answer with the same language and phrase already exists.")

    class Meta:
        ordering = ("action", "language")
        verbose_name = _("answer")
        verbose_name_plural = _("answers")
