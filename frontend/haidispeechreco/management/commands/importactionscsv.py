# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from haidispeechreco.models import Action, Answer, Keyphrase, Language
import csv
import os


class Command(BaseCommand):
    help = "Import actions to the database from a .csv file"

    def add_arguments(self, parser):
        parser.add_argument("-l", "--language", type=str, default="en-EN", required=False)
        parser.add_argument("filename", type=str)

    def handle(self, *args, **kw):
        if not os.path.exists(kw["filename"]):
            raise Exception("File does not exist")
        lang = Language.objects.get(api_code=kw["language"])
        _f = open(kw["filename"])
        reader = csv.reader(_f)
        for row in reader:
            print(row)
            if "=importer_ignore_line" in row:
                print("Skipping this line (=importer_ignore_line)")
                continue
            try:
                act = Action.objects.get(name=row[0])
            except:
                act = Action.objects.create(name=row[0], emotion=row[2])
                print("Created acton", act.name)
            try:
                resp = Answer.objects.get(action=act, language=lang, answer=row[1])
            except:
                if row[1]:
                    resp = Answer.objects.create(action=act, language=lang, answer=row[1])
                    print("Created answer", resp.answer)
            for kp in row[3:]:
                if kp:
                    try:
                        _kp = Keyphrase.objects.create(action=act, language=lang, phrase=kp)
                        print("Created keyphrase", _kp.phrase)
                    except:
                        pass
        _f.close()
