# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
from .models import Action, Keyphrase, Answer
from . import forms


class ActionsList(LoginRequiredMixin, ListView):
    model = Action
    template_name = "hsr/actions.html"


class ActionDetail(LoginRequiredMixin, DetailView):
    model = Action
    template_name = "hsr/action.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["keyphrases"] = Keyphrase.objects.filter(action=context["object"])
        context["answers"] = Answer.objects.filter(action=context["object"])
        return context


class ActionCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Action
    form_class = forms.EditActionForm
    template_name = "hsr/action_create.html"
    success_message = _("Action created successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.pk})


class ActionEdit(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Action
    form_class = forms.EditActionForm
    template_name = "hsr/action_edit.html"
    success_message = _("Action edited successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.pk})


class ActionDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Action
    template_name = "hsr/action_delete.html"
    success_url = reverse_lazy("hsr:actions-list")
    success_message = _("Action deleted successfully.")


class KeyphraseCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Keyphrase
    form_class = forms.EditKeyphraseForm
    template_name = "hsr/action_create.html"
    success_message = _("Keyphrase created successfully.")

    def get_initial(self):
        act = get_object_or_404(Action, pk=self.kwargs.get("pk"))
        return {"action": act}

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class KeyphraseEdit(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Keyphrase
    form_class = forms.EditKeyphraseForm
    template_name = "hsr/keyphrase_edit.html"
    success_message = _("Keyphrase edited successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class KeyphraseDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Keyphrase
    template_name = "hsr/keyphrase_delete.html"
    success_message = _("Keyphrase deleted successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class AnswerCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Answer
    form_class = forms.EditAnswerForm
    template_name = "hsr/answer_create.html"
    success_message = _("Answer created successfully.")

    def get_initial(self):
        act = get_object_or_404(Action, pk=self.kwargs.get("pk"))
        return {"action": act}

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class AnswerEdit(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Answer
    form_class = forms.EditAnswerForm
    template_name = "hsr/answer_edit.html"
    success_message = _("Answer edited successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class AnswerDelete(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Answer
    template_name = "hsr/answer_delete.html"
    success_message = _("Answer deleted successfully.")

    def get_success_url(self):
        return reverse_lazy("hsr:action-detail", kwargs={"pk": self.object.action.pk})


class KeyphrasesList(LoginRequiredMixin, ListView):
    model = Keyphrase
    template_name = "hsr/keyphrases.html"
