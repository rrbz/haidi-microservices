# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path
from . import views

urlpatterns = [
    path('actions', views.ActionsList.as_view(), name='actions-list'),
    path('actions/create', views.ActionCreate.as_view(), name='action-create'),
    path('actions/<int:pk>/edit', views.ActionEdit.as_view(), name='action-edit'),
    path('actions/<int:pk>/detail', views.ActionDetail.as_view(), name='action-detail'),
    path('actions/<int:pk>/delete', views.ActionDelete.as_view(), name='action-delete'),
    path('actions/<int:pk>/keyphrase/create', views.KeyphraseCreate.as_view(), name='keyphrase-create'),
    path('keyphrases/<int:pk>/edit', views.KeyphraseEdit.as_view(), name='keyphrase-edit'),
    path('keyphrases/<int:pk>/delete', views.KeyphraseDelete.as_view(), name='keyphrase-delete'),
    path('actions/<int:pk>/answer/create', views.AnswerCreate.as_view(), name='answer-create'),
    path('answers/<int:pk>/edit', views.AnswerEdit.as_view(), name='answer-edit'),
    path('answers/<int:pk>/delete', views.AnswerDelete.as_view(), name='answer-delete'),
    path('keyphrases', views.KeyphrasesList.as_view(), name='keyphrases-list'),
]
