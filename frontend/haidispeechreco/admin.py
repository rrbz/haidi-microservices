from django.contrib import admin
from .models import *

admin.site.register(Language)
admin.site.register(Action)
admin.site.register(Keyphrase)
admin.site.register(Answer)
