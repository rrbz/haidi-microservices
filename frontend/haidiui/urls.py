# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin
from django.urls import path, include
import os
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view(), name='home'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('login/logout', views.LogoutView.as_view(), name='logout'),
    path('debug', views.Debug.as_view(), name='debug'),
    path('screen', views.MainScreen.as_view(), name='screen'),
    path('hsr/', include(("haidispeechreco.urls", "hsr"), namespace="hsr")),
]


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
SPOOL_DIR = os.path.join(BASE_DIR, "spool")
PIDFILE = os.path.join(SPOOL_DIR, "frontend_pidfile")
_pid = open(PIDFILE, "w")
_pid.write(str(os.getpid()))
_pid.close()
