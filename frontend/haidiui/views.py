# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import redirect, render
from . import forms
from haidispeechreco.models import Language


class Debug(LoginRequiredMixin, TemplateView):
    template_name = "ui/debug.html"

    def get_context_data(self):
        return {"langs": Language.objects.all()}


class MainScreen(TemplateView):
    template_name = "ui/screen.html"


class LoginView(FormView):
    template_name = "login.html"
    form_class = forms.LoginForm

    def form_valid(self, form):
        user = authenticate(
            self.request,
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"]
        )
        if user is not None:
            login(self.request, user)
            messages.success(self.request, _("Welcome, %s" % (user.get_full_name() or user.username)))
        else:
            messages.warning(self.request, _("Username or password incorrect."))
            return self.render_to_response({"form": self.form_class})

        return redirect("home")


class LogoutView(LoginRequiredMixin, View):
    def __init__(self, *args, **kw):
        self.post = self.get
        return super().__init__(*args, **kw)

    def get(self, request):
        logout(request)
        return redirect("home")


class IndexView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect("debug")
        return render(request, "index.html")
