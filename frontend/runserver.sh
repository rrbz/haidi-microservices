#!/bin/bash

SOURCE=$(dirname "${BASH_SOURCE[0]}")/manage.py
OUT=$(dirname "${BASH_SOURCE[0]}")/../spool/speech-reco_output
PIDFILE=$(dirname "${BASH_SOURCE[0]}")/../spool/frontend_pidfile

python3 $SOURCE runserver 2>&1 > $OUT
rm $PIDFILE
