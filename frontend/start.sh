#!/bin/bash

SOURCE=$(dirname "${BASH_SOURCE[0]}")/runserver.sh
OUT=$(dirname "${BASH_SOURCE[0]}")/../spool/frontend_output

$SOURCE 2>&1 > $OUT &
echo OK
