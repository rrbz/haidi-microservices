#!/bin/bash

FILES=$(find .|grep -v static|grep -v ".git"|grep -v pyc|grep -v xml|grep -v dataset|grep -v spool|grep -v venv|grep -v "db.sqlite3"|grep -v 'migrations'|grep -v 'base64'|grep -v 'django.mo'|grep -v 'mp4'|grep -v pickle|grep -v wav|grep -v jpeg|grep -v jpg|grep -v png|sed -n '1!p'|xargs)

cat $FILES | wc -l
