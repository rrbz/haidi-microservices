#!/usr/bin/python3
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import signal
import sys
import numpy as np
import cv2
import base64
import haidi_client
from faceslib import Face
import face_recognition
import pickle
from datetime import datetime, timedelta
import time
import follower


# Define if debugging
DEBUG = "--debug" in sys.argv
DRAW_PROCESSING_LINES = "--draw-major-dots" in sys.argv


def area(face):
    return face.area()


class Recognitor:
    def __init__(self):
        self.HC = haidi_client.Communicator(haidi_client.SOCK_URL)
        FACERECO_DIR = os.path.dirname(os.path.abspath(__file__))
        fh = open(os.path.join(FACERECO_DIR, "encodings.pickle"), "rb")
        self.data = pickle.loads(fh.read())
        fh.close()
        try:
            import dlib
            CUDA = dlib.DLIB_USE_CUDA
        except:
            CUDA = False
        if CUDA:
            self.model = "cnn"
            self.stream = cv2.VideoCapture(1)
        else:
            self.model = "hug"
            self.stream = cv2.VideoCapture(-1)
        self.start = datetime.now()
        self.frames = 0
        print("Starting HAIDI's face recognition system")
        print("Faces search model:", self.model)


    def loop(self):
        while True:
            # read from camera
            ret, image = self.stream.read()
            rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            # search for faces
            boxes = face_recognition.face_locations(rgb, model=self.model)
            faces = [Face(*x) for x in boxes]
            try:
                major = max(faces, key=area)
            except:
                major = None
            # try to match the biggest face with a person
            if major:
                encoding = face_recognition.face_encodings(rgb, [major.asdots()])[0]
                matches = face_recognition.compare_faces(self.data["encodings"], encoding)
                if any(matches):
                    matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                    counts = {}
                    for i in matchedIdxs:
                        name = self.data["names"][i]
                        counts[name] = counts.get(name, 0) + 1
                    major.faceID = max(counts, key=counts.get)
            # render
            for (top, right, bottom, left) in boxes:
                color = (0, 255, 0)
                if (top, right, bottom, left) == major.asdots():
                    color = (255, 0, 0)
                    y = top - 15 if top - 15 > 15 else top + 15
                    cv2.putText(image, major.faceID, (left, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.75, color, 2)
                cv2.rectangle(image, (left, top), (right, bottom), color, 2)
            # compute right/left
            direction = "h"
            try:
                if major:
                    direction = follower.compute_follow_face(image, major)
            except:
                pass
            # compute fps
            self.frames += 1
            try:
                timediff = (datetime.now()-self.start).total_seconds()
                fps = float(self.frames)/timediff
            except ZeroDivisionError:
                fps = self.frames
            # send to backend
            retval, buff = cv2.imencode(".jpg", image)
            enc_img = base64.b64encode(buff)
            _faces = [f.aslist() for f in faces]
            rsp = self.HC.jsonSend({
                "last_image": enc_img.decode("utf-8"),
                "faces": _faces,
                "fps": fps,
                "direction": direction
            })
            # extra drawing
            if DRAW_PROCESSING_LINES:
                cv2.line(
                    image,
                    (int(image.shape[1]/2), 0),
                    (int(image.shape[1]/2), image.shape[0]),
                    (255, 255, 255),
                    5
                )
                cv2.line(
                    image,
                    (int(image.shape[1]/2)-follower.TOLERANCE, 0),
                    (int(image.shape[1]/2)-follower.TOLERANCE, image.shape[0]),
                    (0, 0, 255),
                    5
                )
                cv2.line(
                    image,
                    (int(image.shape[1]/2)+follower.TOLERANCE, 0),
                    (int(image.shape[1]/2)+follower.TOLERANCE, image.shape[0]),
                    (0, 0, 255),
                    5
                )
                if major:
                    cv2.circle(
                        image,
                        (int(major.center[1]), int(major.center[0])),
                        10, (255, 0, 0), -1
                    )
            # debug part
            if DEBUG:
                print("Frame size:", image.shape)
                print("Direction:", direction)
                print("FPS:", fps)
                if major:
                    print("Major face center:", major.center)
                cv2.imshow('img', image)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            # try to write an fps limiter
            expected_fps = 1
            now = datetime.now()
            expected_totaltime = self.frames/expected_fps
            if self.start + timedelta(seconds=expected_totaltime) > now:
                towait = (self.start + timedelta(seconds=expected_totaltime))-now
                time.sleep(towait.total_seconds())


    def shutdown(self, *args, **kw):
        print("\nGot shutdown signal")
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        SPOOL_DIR = os.path.join(BASE_DIR, "spool")
        PIDFILE = os.path.join(SPOOL_DIR, "face-reco_pidfile")
        os.remove(PIDFILE)
        self.HC.close()
        self.stream.release()
        sys.exit(0)


if __name__ == "__main__":
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    SPOOL_DIR = os.path.join(BASE_DIR, "spool")
    PIDFILE = os.path.join(SPOOL_DIR, "face-reco_pidfile")
    _pid = open(PIDFILE, "w")
    _pid.write(str(os.getpid()))
    _pid.close()
    rd = Recognitor()
    signal.signal(signal.SIGTERM, rd.shutdown)
    rd.loop()
    rd.shutdown()
