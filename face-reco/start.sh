#!/bin/bash

SOURCE=$(dirname "${BASH_SOURCE[0]}")/app.py
OUT=$(dirname "${BASH_SOURCE[0]}")/../spool/face-reco_output

python3 $SOURCE 2>&1 > $OUT &
echo OK
