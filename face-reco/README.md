# Face Reco, a part of Haidi's software

This software should parse the incoming images from the webcam, discover
faces in the image and try to recognize via neural network the faces.

The current user should be even sent to the main app.

# Sources

The whole face recognition and identification is adapted by the tutorial by Adrian@PyImageSearch
