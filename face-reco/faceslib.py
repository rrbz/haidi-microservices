#!/usr/bin/python3
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class FaceLibException(Exception):
    pass


class InvalidList(FaceLibException):
    pass


class Face:
    def __init__(self, topX, topY, bottomX, bottomY, centerX=None, centerY=None, faceID=None):
        self.topX = topX
        self.topY = topY
        self.bottomX = bottomX
        self.bottomY = bottomY
        self.top = (topX, topY)
        self.bottom = (bottomX, bottomY)
        if (not centerX) or (not centerY):
            self.center = ((topX+bottomX)/2, (topY+bottomY)/2)
        else:
            self.center = (centerX, centerY)
        self.faceID = faceID and faceID or "Unknown"


    def aslist(self):
        return [
            int(self.topX),
            int(self.topY),
            int(self.bottomX),
            int(self.bottomY),
            float(self.center[0]),
            float(self.center[1]),
            self.faceID
        ]


    def fromlist(_list):
        if len(_list) < 5:
            raise InvalidList
        return Face(*_list)


    def asdots(self):
        return (self.topX, self.topY, self.bottomX, self.bottomY)


    def area(self):
        return (self.bottomX-self.topX)*(self.topY-self.bottomY)
