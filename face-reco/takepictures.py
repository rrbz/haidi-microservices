#!/usr/bin/python3
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cv2
import face_recognition


class TakePicture:
    def __init__(self):
        self.model = "hog"
        self.stream = cv2.VideoCapture(0)
        self.frame = 0

    def loop(self):
        while True:
            ret, image = self.stream.read()
            rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            boxes = face_recognition.face_locations(rgb, model=self.model)
            for (top, right, bottom, left) in boxes:
                color = (0, 255, 0)
                cv2.rectangle(image, (left, top), (right, bottom), color, 2)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('c'):
                print("Saving", self.frame)
                cv2.imwrite("user/img%s.png" % self.frame, image)
            if key == ord('q'):
                break
            cv2.imshow('img', image)
            self.frame += 1


if __name__ == "__main__":
    TakePicture().loop()
