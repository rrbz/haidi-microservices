# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


TOLERANCE = 75


def compute_follow_face(image, main_face):
    w, h, c = image.shape
    center = h/2
    tolerance = TOLERANCE
    if center-tolerance <= main_face.center[1] <= center+tolerance:
        print(center-tolerance, "<=", main_face.center[1], "<=", center+tolerance)
        return "h"
    print(main_face.center[1], center)
    if main_face.center[1] < center:
        return "s"
    return "d"
