#!/bin/bash


for i in backend frontend speech-reco face-reco; do
	if [ -d $i ]; then
		if [ -e $i/start.sh ]; then
			echo -n Starting $i "... "
			$i/start.sh
			sleep 2
		fi
	fi
done
