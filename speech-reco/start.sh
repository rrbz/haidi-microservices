#!/bin/bash

SOURCE=$(dirname "${BASH_SOURCE[0]}")/microphone.py
OUT=$(dirname "${BASH_SOURCE[0]}")/../spool/speech-reco_output

python3 $SOURCE 2>&1 > $OUT &
echo OK
