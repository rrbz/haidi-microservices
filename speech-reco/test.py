import speech_recognition as sr

LANG = "en-US"

r = sr.Recognizer()
test = sr.AudioFile("tmp.wav")
with test as source:
    audio = r.record(source)

print("GOOGLE thinks you said", r.recognize_google(audio, language=LANG))
print("SPHINX thinks you said", r.recognize_sphinx(audio, language=LANG))
