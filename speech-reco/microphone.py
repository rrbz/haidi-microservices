# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import signal
import speech_recognition as sr
import requests
import json
import haidi_client
import time
import json


class Recorder:
    def __init__(self):
        self.HC = haidi_client.Communicator(haidi_client.SOCK_URL)
        self.r = sr.Recognizer()
        self.calibrate()


    def calibrate(self, duration=5):
        print("Calibrating")
        self.HC.jsonSend({"micstate": "calibrating"})
        with sr.Microphone(device_index=11) as source:
            self.r.adjust_for_ambient_noise(source, duration=duration)


    def loop(self):
        self.HC.jsonSend({"micstate": "ready"})
        while True:
            print("Query")
            do = self.HC.jsonSend({"micop": "?"})
            print("Parse", do)
            data = json.loads(do)
            if data["calibrate"]:
                self.calibrate()
            if data["record"]:
                with sr.Microphone(device_index=11) as source:
                    self.HC.jsonSend({"micstate": "recording"})
                    print("Listen")
                    audio = self.r.listen(source, timeout=7)
                    self.HC.jsonSend({"micstate": "transcribing"})
                    print("Transcribe")
                    engine = getattr(self.r, "recognize_%s" % data["engine"])
                    lang = (data["engine"] == "sphinx" and data["language"] == "en-EN") and "en-US" or data["language"]
                    try:
                        phrase = engine(audio, language=lang)
                    except sr.UnknownValueError:
                        phrase = ""
                    print(phrase)
                    self.HC.jsonSend({"last_phrase": phrase})
            self.HC.jsonSend({"micstate": "ready"})
            print("Wait")
            time.sleep(0.5)


    def shutdown(self, *args, **kw):
        print("\nGot shutdown signal")
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        SPOOL_DIR = os.path.join(BASE_DIR, "spool")
        PIDFILE = os.path.join(SPOOL_DIR, "speech-reco_pidfile")
        os.remove(PIDFILE)
        self.HC.close()
        sys.exit(0)


if __name__ == "__main__":
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    SPOOL_DIR = os.path.join(BASE_DIR, "spool")
    PIDFILE = os.path.join(SPOOL_DIR, "speech-reco_pidfile")
    _pid = open(PIDFILE, "w")
    _pid.write(str(os.getpid()))
    _pid.close()
    rd = Recorder()
    signal.signal(signal.SIGTERM, rd.shutdown)
    rd.loop()
