#define pinPulsante 4
#define pinPot A0
float potAngle = 0;
enum {
  PIN_DIR = 2,
  PIN_STEP = 3
};
int dlay = 10;
float actualAngle = 75;



long lastCall = 0;
char a;
int orizzontale = 0;
int luce = 0;

void setup() {
  Serial.begin(115200);

  //motore movimento testa orizzontale
  pinMode(PIN_STEP, OUTPUT);
  pinMode(PIN_DIR, OUTPUT);

  //luci
  pinMode(4, OUTPUT);
  //pinMode(5, OUTPUT);
  //pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  //pinMode(11, OUTPUT);

  //motore movimento testa verticale

}

void loop() {
  comunicazione();
  motoreOrizzontale();
  //motoreVerticale();
  luci();
}





void comunicazione () {
  if (Serial.available () > 0){
    a = Serial.read();
    lastCall=millis();
    //Serial.println(luce);
    switch (a) { 
    case 115:      //d = 100   gira la testa a destra
      orizzontale = 2;
      //Serial.println(a);
      a = ' ';
      break;
    case 100:        //s = 115    gira la testa a sinistra
      orizzontale = 1;
      //Serial.println(a);
      a = ' ';
      break;
    case 104:      //h = 104       sta ferma lungo l'asse verticale
      orizzontale = 0;
      //Serial.println(a);
      a = ' ';
      break;
    case 111:    //o = 111     sale
      //programma per salire
      a = ' ';
      break;
    case 117:      // u = 117  scende
      //programma per scendere
      a = ' ';
      break;
    case 102:     //f = 102 sta ferma lungo l'asse orizzontale
      //programma per stare ferma
      a = ' ';
      break;
    case 49:  //1
      luce = 1; 
      a = ' ';
      break;
    case 50:  //2
      luce = 2;
      a = ' ';
      break;
    case 51:  //3
      luce = 3;
      a = ' ';
      break;
    case 52:  //4
      luce = 4;
      a = ' ';
      break;
    case 53:   //5
      luce = 5;
      a = ' ';
      break;

    }
  }
  //if(millis()-lastCall<500){
  //orizzontale = 0;
  //}
}



void luci () {
  if (luce == 1) {
    digitalWrite(4, HIGH);
    digitalWrite(7, HIGH);
    digitalWrite(8, HIGH);
    digitalWrite(12, HIGH);
    digitalWrite(13, HIGH);
  }
  else if (luce == 2) {
    digitalWrite(4, LOW);
    digitalWrite(7, LOW);
    digitalWrite(8, LOW);
    digitalWrite(12, LOW);
    digitalWrite(13, LOW);
  }
  else if (luce == 3) {           //TRISTE
    digitalWrite(4, HIGH);
    digitalWrite(7, HIGH);
    digitalWrite(8, LOW); //sotto singolo
    digitalWrite(12, HIGH); //SOTTO
    digitalWrite(13, LOW); //SOPRA
  }
  else if (luce == 4) { //NORMALE
    digitalWrite(4, HIGH);
    digitalWrite(7, LOW);
    digitalWrite(8, HIGH);
    digitalWrite(12, HIGH);
    digitalWrite(13, LOW);
  }
  else if (luce == 5) {  //ARRABBIATO
    digitalWrite(4, HIGH);
    digitalWrite(7, LOW);
    digitalWrite(8, HIGH);
    digitalWrite(12, LOW);
    digitalWrite(13, HIGH);
  }
}





void motoreOrizzontale () {
  //  if (orizzontale == 0) {
  //    digitalWrite(PIN_DIR, HIGH);
  //    digitalWrite(PIN_STEP, HIGH); 
  //    actualAngle = actualAngle;   
  //
  //    /*digitalWrite(7, LOW);
  //    digitalWrite(8, LOW);
  //    digitalWrite(12, LOW);
  //    digitalWrite(13, LOW);*/
  //  }
  if (orizzontale == 1 && actualAngle -1.8 > 0) {
    orizzontale = 0;
    digitalWrite(PIN_DIR, HIGH);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW); 
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW); 
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW); 
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW); 
    delay(dlay);
    actualAngle -= 7.2;
  }

  else if (orizzontale == 2 && actualAngle +1.8 < 150) {
    orizzontale = 0;
    digitalWrite(PIN_DIR, LOW);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW);
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW);
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW);
    delay(dlay);
    digitalWrite(PIN_STEP, HIGH); 
    delay(dlay);
    digitalWrite(PIN_STEP, LOW);
    delay(dlay);
    actualAngle += 7.2;
  }
  //
  //  else {
  //    digitalWrite(PIN_DIR, HIGH);
  //    digitalWrite(PIN_STEP, HIGH);
  //    }
}

