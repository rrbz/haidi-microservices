# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import signal


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SPOOL_DIR = os.path.join(BASE_DIR, "spool")


class TerminatedError(Exception):
    pass


class MicroService:
    def __init__(self, slug, label, pidfile):
        self.slug = slug
        self.label = label
        self.pidfile = pidfile
        self.terminating = False


    def read_pid(self):
        try:
            f = open(os.path.join(SPOOL_DIR, self.pidfile))
        except FileNotFoundError:
            self.pid = None
            return None
        self.pid = int(f.read())
        f.close()
        return self.pid


    def start(self):
        STARTSCRIPT = os.path.join(BASE_DIR, self.slug, "start.sh")
        out = os.system(STARTSCRIPT)
        if out == 0:
            return [True, "%s service has been started and is now running." % (self.label)]
        return [False, "%s start script exited with a non-zero code (%s)." % (self.label, out)]


    def stop(self):
        if not self.read_pid():
            return [False, "%s service is not running." % (self.label)]
        if not self.is_alive():
            return [False, "%s service is not running." % (self.label)]
        try:
            os.kill(self.pid, signal.SIGTERM)
            self.terminating = True
        except Exception as e:
            return [False, "Error while terminating %s: %s" %(self.label, e)]
        return [True, "%s shutted down correctly." % self.label]


    def is_alive(self):
        try:
            os.kill(self.pid, 0)
            return True
        except OSError:
            os.remove(os.path.join(SPOOL_DIR, self.pidfile))
            raise TerminatedError
        return False


    def status(self, interactor):
        if not self.read_pid():
            return {"slug": self.slug, "label": self.label, "pid": None, "alive": False}
        try:
            alive = self.is_alive()
        except TerminatedError:
            alive = False
            if self.terminating:
                self.terminating = False
            else:
                interactor.notify("warning", "%s (%s) unexpectedly exited and entered fail state." % (
                    self.label, self.pid
                ))
        return {"slug": self.slug, "label": self.label, "pid": self.pid, "alive": alive}



backend = MicroService("backend", "Backend", "backend_pidfile")
speech_reco = MicroService("speech-reco", "Speech Recognition", "speech-reco_pidfile")
face_reco = MicroService("face-reco", "Face Recognition", "face-reco_pidfile")
frontend = MicroService("frontend", "Frontend", "frontend_pidfile")

services = [backend, speech_reco, face_reco, frontend]


def status(interactor):
    states = []
    for i in services:
        states.append(i.status(interactor))
    return states


def start_service(slug):
    for srv in services:
        if srv.slug == slug:
            return srv.start()
    return False


def stop_service(slug):
    for srv in services:
        if srv.slug == slug:
            return srv.stop()
    return False
