# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import utils
import random
import pyttsx3
import json
import time
import serial


class Interactor:
    phrases = []
    ready = False
    ScreenSocket = None
    FrontendSocket = None
    language = "en-EN"
    record = True
    calibrate = False
    speech_reco_engine = "sphinx"
    faces = []
    fps = None


    def __init__(self):
        # load and set a default image base64 encoded
        BACKEND_DIR = os.path.dirname(os.path.abspath(__file__))
        IMG = os.path.join(BACKEND_DIR, "default_image.base64")
        _img = open(IMG)
        self.last_image = _img.read()
        _img.close()
        self.engine = pyttsx3.init()
        self.engine.setProperty("engine", "en-us+f2")
        self.engine.setProperty("rate", 100)
        self.engine.setProperty("volume", 3)
        dev = "/dev/ttyACM0"
        for i in os.listdir("/dev"):
            if "ttyACM" in i:
                dev = os.path.join("/dev", i)
        try:
            self.ArduinoCommunicator = serial.Serial(dev, 115200)
        except Exception as e:
            print(e)
            self.ArduinoCommunicator = None

    def last_phrase(self):
        try:
            return self.phrases[-1]
        except:
            pass
        return "--- No phrase ---"


    def parse_phrase(self, phrase):
        self.record = False
        self.phrases.append(phrase)
        self.send_last_phrase(processing=True)
        action = utils.process_phrase(phrase, self.language)
        self.send_last_phrase(result=action[2])
        response = None
        images = None
        person = None
        act = None
        if not action[1]:
            if action[4]:
                action[0] = action[3]
                action[1] = action[4]
            else:
                self.record = True
                return
        if len(action[1]) > 1:
            answ = random.choice(action[1])
        else:
            answ = action[1][0]
        if answ.answer:
            response = answ.answer
        if action[0].code:
            output = {}
            try:
                exec(action[0].code, locals(), output)
            except Exception as e:
                self.notify("warning", "Answer's python code error: %s" % e)
            if output.get("response"):
                response = output["response"]
            if output.get("images"):
                images = output["images"]
        if response:
            self._fsm({"text": response})
            self._toarduino(str(action[0].emotion))
            self.engine.say(response)
            self.engine.runAndWait()
        if images:
            _fsm({"images": images})
        time.sleep(1.5)
        self.record = True
        return action


    def set_frontend_socket(self, sock):
        self.FrontendSocket = sock


    def set_screen_socket(self, sock):
        self.ScreenSocket = sock


    def send_last_phrase(self, processing=False, result=None):
        if not self.FrontendSocket:
            return
        data = {"last_phrase": self.last_phrase()}
        if processing:
            data["micstate"] = "processing"
        if result:
            this = ""
            for i in result:
                this += "%s (%s)\n" % (i[0], i[1])
            data["speech_reco-status"] = this
        self.FrontendSocket.jsonSend(data)


    def send_last_image(self):
        if not self.FrontendSocket:
            return
        data = {"last_image": self.last_image, "faces": 0, "person": None}
        if self.faces:
            data["faces"] = len(self.faces)
        for face in self.faces:
            if face.faceID:
                data["person"] = face.faceID
        if self.fps != None:
            data["fps"] = self.fps
        self.FrontendSocket.jsonSend(data)


    def notify(self, weight, text):
        if not self.FrontendSocket:
            return
        self.FrontendSocket.jsonSend({"msg": {"weight": weight, "text": text}})


    def set_microphone_status(self, state):
        if state in ["calibrating", "recording", "ready", "transcribing"]:
            self.MicStatus = state
            if self.FrontendSocket:
                self.FrontendSocket.jsonSend({"micstate": self.MicStatus})
            if state in ["calibrating", "recording", "transcribing"]:
                self._fsm({state: True})


    def micstate(self):
        data = {}
        data["record"] = self.record
        if not self.ready or len(self.faces) == 0:
            data["record"] = False
        if len(self.faces) == 0 and self.record:
            self._fsm({"micready": True})
        data["calibrate"] = self.calibrate
        data["language"] = self.language
        data["engine"] = self.speech_reco_engine
        if self.calibrate:
            self.calibrate = False
        return json.dumps(data)


    def set_engine(self, engine):
        if engine not in ["sphinx", "google"]:
            self.notify("danger", "Recived an invalid value for speech recognition engine.")
            return
        self.speech_reco_engine = engine
        self.notify("success", "Speech recognition engine changed to %s" % engine)


    # Send a json message to screen if socket is defined
    def _fsm(self, obj):
        if not self.ScreenSocket:
            return
        self.ScreenSocket.jsonSend(obj)


    # Send messages to arduino
    def _toarduino(self, s):
        if not self.ArduinoCommunicator:
            return None
        try:
            self.ArduinoCommunicator.write(s.encode("utf-8"))
        except:
            pass
        return None
