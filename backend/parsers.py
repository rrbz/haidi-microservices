# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import utils
from faceslib import Face


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def LastPhraseParser(data, interactor, client):
    client.jsonSend({"ack": "ok"})
    interactor.parse_phrase(data["last_phrase"])
    return


def LastImageParser(data, interactor, client):
    interactor.last_image = data["last_image"]
    interactor.faces = [Face.fromlist(l) for l in data["faces"]]
    interactor.fps = data.get("fps", None)
    direction = data.get("direction", "h")
    if direction != "h":
        interactor._toarduino(direction)
    interactor.send_last_image()
    return "ok"


def FrontendParser(data, interactor, client):
    interactor.set_frontend_socket(client)
    interactor.send_last_phrase()
    interactor.send_last_image()
    return '{"frontend": "ok"}'


def HostStatusParser(data, interactor, client):
    data = utils.host_info(interactor)
    data["ready"] = interactor.ready
    data["language"] = interactor.language
    data["engine"] = interactor.speech_reco_engine
    return json.dumps({"host_status": data})


def StartParser(data, interactor, client):
    r = utils.microservices.start_service(data["start"])
    if r[0]:
        interactor.notify("success", r[1])
    else:
        interactor.notify("warning", r[1])
    print("Started service", data["start"])


def StopParser(data, interactor, client):
    r = utils.microservices.stop_service(data["stop"])
    if r[0]:
        interactor.notify("success", r[1])
    else:
        interactor.notify("warning", r[1])
    print("Stopped service", data["stop"])


def SetLanguageParser(data, interactor, client):
    interactor.language = data["setlang"]
    interactor.notify("success", "Active language successfully set to %s" % interactor.language)
    return '{"ack": "ok"}'


def MicrophoneStateParser(data, interactor, client):
    interactor.set_microphone_status(data["micstate"])
    return '{"ack": "ok"}'


def MicrophoneOperationParser(data, interactor, client):
    return interactor.micstate()


def SetEngineParser(data, interactor, client):
    interactor.set_engine(data["seteng"])
    return '{"ack": "ok"}'


def SetCalibrationRequestParser(data, interactor, client):
    interactor.calibrate = True
    interactor.notify("success", "Calibration request successfully sent to speech reco microservice")
    return '{"ack": "ok"}'


def ScreenParser(data, interactor, client):
    interactor.set_screen_socket(client)
    return '{"ack": "ok"}'


def SetScreenReadyParser(data, interactor, client):
    interactor.ready = data["setready"]
    return '{"ack": "ok"}'


def IsReadyParser(data, interactor, client):
    return json.dumps({"host_status": {"ready": interactor.ready}})


def UpdateParser(data, interactor, client):
    interactor.notify("warning", "Updating in progress... it will be all stopped.")
    os.system(os.path.join(BASE_DIR, "deploy.sh"))


def SystemctlParser(data, interactor, client):
    action = data["systemctl"]
    if action not in ["poweroff", "reboot"]:
        return '{"ack": "ok"}'
    os.system("systemctl %s" % action)


def ArduinoDoParser(data, interactor, client):
    act = data.get("arduinodo")
    if act in ["d", "s", "1", "2", "3", "4", "5"]:
        interactor._toarduino(act)
    return '{"ack": "ok"}'


p = {
    "last_phrase": LastPhraseParser,
    "last_image": LastImageParser,
    "frontend": FrontendParser,
    "host_status": HostStatusParser,
    "start": StartParser,
    "stop": StopParser,
    "setlang": SetLanguageParser,
    "micstate": MicrophoneStateParser,
    "micop": MicrophoneOperationParser,
    "seteng": SetEngineParser,
    "setcalibration": SetCalibrationRequestParser,
    "screen": ScreenParser,
    "setready": SetScreenReadyParser,
    "isready": IsReadyParser,
    "deploy": UpdateParser,
    "systemctl": SystemctlParser,
    "arduinodo": ArduinoDoParser,
}


def parse(data, interactor, client):
    response =  []
    for key in data.keys():
        if key in p.keys():
            r = p[key](data, interactor, client)
            if r:
                if r:
                    response.append(r)
        else:
            print("Unknown keyword %s" % key)
    if response:
        for i in response:
            client.sendMessage(i.encode('utf-8'))
    return response
