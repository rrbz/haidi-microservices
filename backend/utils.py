# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import psutil
import platform
import microservices
from humanize import naturalsize as size
from fuzzywuzzy import process
import djangoapi as db


def host_info(interactor):
    data = {}
    vm = psutil.virtual_memory()
    data["ram_perc"] = vm.percent
    data["ram"] = "%s / %s" % (size(vm.total-vm.available), size(vm.total))
    data["cpu_perc"] = psutil.cpu_percent()
    ifaces = {}
    net = psutil.net_if_addrs()
    for i in net:
        if i == "lo":
            continue
        this = {}
        this["ip"] = net[i][0].address
        this["netmask"] = net[i][0].netmask
        this["broadcast"] = net[i][0].broadcast
        ifaces[i] = this
    data["interfaces"] = ifaces
    sensors = psutil.sensors_temperatures()
    temp = []
    for s in sensors:
        for i in sensors[s]:
            _t = i._asdict()
            if not _t["label"]:
                _t["label"] = s
            if not _t["critical"]:
                _t["critical"] = 100
            if not _t["high"]:
                _t["high"] = _t["critical"]
            temp.append(_t)
    data["temperatures"] = temp
    pt = platform.uname()
    data["platform"] = "%s %s %s %s - %s" % (
        pt.system, pt.machine, pt.release, pt.version, pt.node
    )
    data["services"] = microservices.status(interactor)

    return data


def process_phrase(phrase, lang, threshold=70):
    lang = db.Language.objects.get(api_code=lang)
    keys = db.Keyphrase.objects.filter(language=lang).values_list("phrase", flat=True)
    choices = list(keys)
    result = process.extract(phrase, choices, limit=3)
    action = None
    answers = None
    if result[0][1] > threshold:
        action = db.Keyphrase.objects.get(language=lang, phrase=result[0][0]).action
        answers = db.Answer.objects.filter(language=lang, action=action)
    try:
        noaction = db.Action.objects.get(name="NXACTION")
        noansw = db.Answer.objects.filter(language=lang, action=noaction)
    except:
        noansw = []
    return [action, answers, result, noaction, noansw]
