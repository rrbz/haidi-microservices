# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import signal
import json
from autobahn.twisted.websocket import WebSocketServerProtocol
from twisted.python import log
from twisted.internet import reactor
from autobahn.twisted.websocket import WebSocketServerFactory
from interaction import Interactor
from parsers import parse


interactor = Interactor()


class CommunicationError(Exception):
    pass


class HAIDIsServerProtocol(WebSocketServerProtocol):
    def onConnect(self, request):
        print("Client connecting: {}".format(request.peer))

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        if isBinary:
            raise CommunicationError("Got binary payload")
        msg = payload.decode('utf8')
        print("Text message received: {}".format(msg))
        try:
            data = json.loads(msg)
        except Exception as e:
            raise CommunicationError("Got a non-JSON payload (%s)" % e)
        reactor.callInThread(parse, data, interactor, self)

    def jsonSend(self, payload):
        p = json.dumps(payload)
        self.sendMessage(p.encode("utf-8"))

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {}".format(reason))


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SPOOL_DIR = os.path.join(BASE_DIR, "spool")
PIDFILE = os.path.join(SPOOL_DIR, "backend_pidfile")
_pid = open(PIDFILE, "w")
_pid.write(str(os.getpid()))
_pid.close()


def shutdown(*args, **kw):
    print("\nGot shutdown signal")
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    SPOOL_DIR = os.path.join(BASE_DIR, "spool")
    PIDFILE = os.path.join(SPOOL_DIR, "backend_pidfile")
    os.remove(PIDFILE)


try:
    log.startLogging(sys.stdout)
    factory = WebSocketServerFactory()
    factory.protocol = HAIDIsServerProtocol
    reactor.listenTCP(9000, factory)
    reactor.run()
except KeyboardInterrupt:
    pass

shutdown()
