#!/bin/bash

SOURCE=$(dirname "${BASH_SOURCE[0]}")/server.py
OUT=$(dirname "${BASH_SOURCE[0]}")/../spool/backend_output

python3 $SOURCE 2>&1 > $OUT &
echo OK
