# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "HAIDI"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import websocket
import json

SOCK_URL = "ws://localhost:9000"


class CommunicationError(Exception):
    pass


class Communicator:
    def __init__(self, sock):
        self.ws = websocket.create_connection(sock)

    def send(self, *args, **kw):
        if not getattr(self, "ws"):
            raise CommunicationError("WebSocket is not initialized")
        self.ws.send(*args, **kw)
        return self.ws.recv()

    def jsonSend(self, jsObj):
        p = json.dumps(jsObj)
        return self.send(p)

    def close(self):
        if not getattr(self, "ws"):
            raise CommunicationError("WebSocket is not initialized")
        self.ws.close()


if __name__ == "__main__":
    w = Communicator(SOCK_URL)
    print(w.send(input("MSG: ")))
    w.close()
