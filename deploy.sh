#!/bin/bash
# Copyright (c) 2019 Marco Marinello
# A simple script to download the latest version from git

cd $(dirname "${BASH_SOURCE[0]}")

git pull


./stop.sh
systemctl restart haidi-backend
